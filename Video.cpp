#include "Video.h"

/**
 * Initialize the core of SDL library
 * @return false if fails
 */
bool Video::Initialize() {
    if(SDL_Init( SDL_INIT_VIDEO ) < 0) {
        printf("SDL Video could not be initialized! Error: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

/**
 * Create the window
 * @return
 */
bool Video::MakeWindow() {
    window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              width, height, SDL_WINDOW_SHOWN);
    if(window == NULL) {
        printf("Failed to create window, Error: %s\n", SDL_GetError());
        return false;
    }

    screen = SDL_GetWindowSurface(window);
    return true;
}

/***
 * Start the window event pipe
 * @param eventHandler
 * @param clockevent
 */
void Video::StartEvent(void (*eventHandler)(Video* video, SDL_Event* event), void (*clockevent)()) {
    while(!done) {
        SDL_Event event;
        while(SDL_PollEvent(&event) != 0){
            eventHandler(this, &event);
        }
        clockevent();
    }
}

/**
 * Draw a box at x y pos
 * @param x
 * @param y
 */
void Video::DrawRectangle(const size_t x, const size_t y) {
    SDL_Rect rect;
    rect.h = SizeY;
    rect.w = SizeX;
    rect.x = x;
    rect.y = y;

    SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 0xFF, 0x20, 0x20));
}

/**
 * Draw a bunch of boxes
 * @param positions
 */
void Video::DrawRectangles(std::vector<Position> positions) {
    std::vector<SDL_Rect> ConvertedRects;
    for(Position pos : positions) {
        SDL_Rect rect;
        rect.h = SizeY;
        rect.w = SizeX;
        rect.x = pos.PosX;
        rect.y = pos.PosY;

        ConvertedRects.push_back(rect);
    }

    SDL_FillRects(screen, &ConvertedRects[0], ConvertedRects.size(), SDL_MapRGB(screen->format, 0xfe, 0xfe, 0xfe));
}

/**
 * Sets window title
 * @param title
 */
void Video::SetTitle(std::string title) {
    SDL_SetWindowTitle(window, title.c_str());
}

/**
 * Render what has been drawn
 */
void Video::Render() {
    SDL_UpdateWindowSurface(window);
}

/**
 * Clear the screen
 */
void Video::Clear() {
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
}

/**
 * Stop the event view
 * @param value
 */
void Video::setIsDone(bool value) {
    this->done = value;
}