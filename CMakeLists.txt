cmake_minimum_required(VERSION 3.14)
project(imasnake)

set(CMAKE_CXX_STANDARD 17)

find_package(SDL2 REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS})

add_executable(imasnake main.cpp Video.cpp Video.h Game.cpp Game.h pch.h)
target_link_libraries(imasnake ${SDL2_LIBRARIES})