#ifndef IMASNAKE_GAME_H
#define IMASNAKE_GAME_H

#include <vector>
#include <string>
#include <ctime>
#include <cmath>
#include <math.h>
#include "pch.h"


class Game {
public:
    void SetBotMode(bool mode);
    void SetDirection(Direction direction);
    Direction  GetDirection();

    bool Move();
    void GenerateFood();
    Position GetFood();

    std::vector<Position> GetTails();

private:
    Direction  direction;
    bool useBot;
    size_t tailLength = 0;
    size_t PosX = 0, PosY = 0;

    Position food;
    std::vector<Position> tail;
};

#endif
