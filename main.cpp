#include "pch.h"
#include "Video.h"
#include "Game.h"

void VideoEvent(Video* video, SDL_Event* event);
void Clock();

std::shared_ptr<Video> video(new Video("Im a snake", 800, 600));
std::shared_ptr<Game> game(new Game());

int main() {
    game->SetDirection(Direction::RIGHT);
    if(video->Initialize()) {
        if(video->MakeWindow()) {
            video->StartEvent(&VideoEvent, &Clock);
        }
    }
    return 0;
}

void VideoEvent(Video* video, SDL_Event* event) {
    switch(event->type) {
        case SDL_QUIT:
            video->setIsDone(true);
            break;
        case SDL_KEYDOWN:
            switch(event->key.keysym.sym) {
                case SDLK_UP:
                    if(game->GetDirection() == Direction::DOWN && game->GetTails().size() > 1) break;
                    game->SetDirection(Direction::UP);
                    break;
                case SDLK_DOWN:
                    if(game->GetDirection() == Direction::UP && game->GetTails().size() > 1) break;
                    game->SetDirection(Direction::DOWN);
                    break;
                case SDLK_LEFT:
                    if(game->GetDirection() == Direction::RIGHT && game->GetTails().size() > 1) break;
                    game->SetDirection(Direction::LEFT);
                    break;
                case SDLK_RIGHT:
                    if(game->GetDirection() == Direction::LEFT && game->GetTails().size() > 1) break;
                    game->SetDirection(Direction::RIGHT);
                    break;
            }
            break;
    }
}

int Tick = 0;
int doMove = true;
size_t score;

void Clock() {
    if(Tick++ == (150000 - (score * 100)) && doMove) {
        Tick = 0;
        score = game->GetTails().size() * 10;
        if(!game->Move()){
            doMove = false;
            char* final = new char[32];
            sprintf(final, "Final Score: %d", score);
            video->SetTitle(std::string(final));
        }

        video->Clear();

        auto tail = game->GetTails();
        video->DrawRectangles(tail);

        Position food = game->GetFood();
        video->DrawRectangle(food.PosX, food.PosY);

        video->Render();
    }
}