#include "Game.h"

void Game::SetBotMode(bool mode) {
    this->useBot = mode;
}

void Game::SetDirection(Direction direction) {
    this->direction = direction;
}

bool Game::Move() {
    switch(direction) {
        case Direction::UP:
            PosY -= SizeY;
            break;
        case Direction ::DOWN:
            PosY += SizeY;
            break;
        case Direction::LEFT:
            PosX -= SizeX;
            break;
        case Direction::RIGHT:
            PosX += SizeX;
            break;
    }

    for(Position wall : tail) {
        if(wall.PosX == PosX && wall.PosY == PosY) {
            printf("death\n");
            return false;
        }
    }

    //TODO Fetch screen size data
    if(PosX > 800) {
        PosX = 0;
    } else if(PosX < 0) {
        PosX = 800;
    }

    if(PosY > 580) {
        PosY = 0;
    } else if(PosY < 0) {
        PosY = 580;
    }

    Position entry {
        PosX,
        PosY
    };

    if(tail.size() > tailLength) {
        tail.erase(tail.begin());
    }

    tail.push_back(entry);

    if(PosX == food.PosX && PosY == food.PosY) {
        printf("Food found\n");
        tailLength++;
        this->GenerateFood();
    }

    return true;
}

Position Game::GetFood() {
    return food;
}

void Game::GenerateFood() {
    srand(time(nullptr));
    food = {
            static_cast<size_t> (round((rand() % 40 * SizeY)/100)*100),
            static_cast<size_t> (round((rand() % 30 * SizeX)/100)*100)
    };

    for(Position wall : tail) {
        if(wall.PosX == food.PosX && wall.PosY == food.PosY) {
            GenerateFood();
            printf("Food tail hit\n");
        }
    }
}

std::vector<Position> Game::GetTails() {
    return tail;
}

Direction Game::GetDirection() {
    return direction;
}

//TODO Re add bot move 
//void BotMove(Position entry) {
//    if(entry.PosY < food.PosY && dir != Direction::UP) {
//        std::cout << "DOWN" << std::endl;
//        dir = Direction::DOWN;
//    } else if(entry.PosY > food.PosY && dir != Direction::DOWN) {
//        std::cout << "UP" << std::endl;
//        dir = Direction::UP;
//    } else if (entry.PosX < food.PosX && dir != Direction::LEFT) {
//        std::cout << "RIGHT" << std::endl;
//        dir = Direction::RIGHT;
//    } else if (entry.PosX > food.PosX && dir != Direction::RIGHT) {
//        std::cout << "LEFT" << std::endl;
//        dir = Direction::LEFT;
//    }
//}