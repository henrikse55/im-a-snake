#ifndef IMASNAKE_VIDEO_H
#define IMASNAKE_VIDEO_H

#include "pch.h"

class Video {
public:
    Video(
            const std::string& title,
            const size_t width,
            const size_t height
            ) {
        this->title = title;
        this->width = width;
        this->height = height;
    }

    ~Video() {
        printf("Destroying windows\n");
        SDL_FreeSurface(this->screen);
        SDL_DestroyWindow(this->window);
        SDL_Quit();
    }

    bool Initialize();
    bool MakeWindow();
    void StartEvent(void (*eventHandler)(Video* video, SDL_Event* event), void (*clockevent)());

    void DrawRectangle(size_t x, size_t y);
    void DrawRectangles(std::vector<Position> positions);

    void Render();
    void Clear();
    void SetTitle(std::string title);

    void setIsDone(bool value);

private:
    // General window information
    std::string title;
    size_t width;
    size_t height;

    // SDL Window pointers
    SDL_Window* window;
    SDL_Surface* screen;

    bool done = false;
};

#endif
