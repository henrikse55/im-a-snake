#ifndef IMASNAKE_PCH_H
#define IMASNAKE_PCH_H

#include <string>
#include <memory>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <SDL2/SDL.h>
#include <vector>

#define SizeX 20
#define SizeY 20

enum Direction {
    UP, DOWN, LEFT, RIGHT
};

struct Position {
    size_t PosX;
    size_t PosY;
};

#endif //IMASNAKE_PCH_H
